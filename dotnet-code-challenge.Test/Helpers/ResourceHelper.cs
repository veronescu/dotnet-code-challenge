﻿using System.IO;
using System.Reflection;
using System.Xml;

namespace DotNetCodeChallenge.Test.Helpers
{
    public class ResourceHelper
    {
        public static string GetContentOfResource(string resourceName)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.Resources.{resourceName}"))
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static XmlDocument GetXmlDocumentFromResource(string resourceName)
        {
            XmlDocument xmlDocument = new XmlDocument();
            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name}.Resources.{resourceName}"))
                xmlDocument.Load(stream);
            return xmlDocument;
        }
    }
}
