using DotNetCodeChallenge.Controllers;
using DotNetCodeChallenge.Models;
using DotNetCodeChallenge.ServiceLogic;
using DotNetCodeChallenge.Test.Helpers;
using NSubstitute;
using System;
using System.Xml;
using Xunit;

namespace DotNetCodeChallenge.Test
{
    public class XmlUnitTests
    {
        /// <summary>
        /// Given that XML data request is received
        /// When the XML content has horse names for all horses
        /// And the XML content has prices for all horses
        /// Then the response is the horse names in ascending order of their associated price
        /// </summary>
        [Fact]
        public void VerifyXmlResultWhenCorrectContent()
        {
            XmlDataExtractor jsonDataExtractor = new XmlDataExtractor();
            XmlDocument xmlDocument = ResourceHelper.GetXmlDocumentFromResource("Caulfield_Race1.xml");
            var result = jsonDataExtractor.GetHorseNamesByAscendingPrice(xmlDocument);
            Assert.Equal(2, result.HorseNames.Count);
            Assert.Equal("Advancing", result.HorseNames[0]);
            Assert.Equal("Coronel", result.HorseNames[1]);
            Assert.Empty(result.ValidationErrorCodes);
        }

        /// <summary>
        /// Given that XML data request is received
        /// When the XML content misses horse names for some horses
        /// And the XML content has prices for all horses
        /// Then the response contains the error code for missing horse names
        /// </summary>
        [Fact]
        public void VerifyXmlResultWhenMissingHorseNames()
        {
            XmlDataExtractor jsonDataExtractor = new XmlDataExtractor();
            XmlDocument xmlDocument = ResourceHelper.GetXmlDocumentFromResource("Caulfield_Race1_MissingHorseName.xml");
            var result = jsonDataExtractor.GetHorseNamesByAscendingPrice(xmlDocument);
            Assert.Equal(2, result.HorseNames.Count);
            Assert.Equal("Advancing", result.HorseNames[0]);
            Assert.Null(result.HorseNames[1]);
            Assert.Single(result.ValidationErrorCodes);
            Assert.Equal(ErrorCode.MissingHorseNames, result.ValidationErrorCodes[0]);
            
        }

        /// <summary>
        /// Given that XML data request is received
        /// When the XML content has horse names for all horses
        /// And the XML content misses prices for some horses
        /// Then the response contains the error code for missing horse prices
        /// </summary>
        [Fact]
        public void VerifyXmlResultWhenMissingPrices()
        {
            XmlDataExtractor jsonDataExtractor = new XmlDataExtractor();
            XmlDocument xmlDocument = ResourceHelper.GetXmlDocumentFromResource("Caulfield_Race1_MissingPrice.xml");
            var result = jsonDataExtractor.GetHorseNamesByAscendingPrice(xmlDocument);
            Assert.Equal(2, result.HorseNames.Count);
            Assert.Single(result.ValidationErrorCodes);
            Assert.Equal(ErrorCode.MissingHorsePrices, result.ValidationErrorCodes[0]);

        }

        /// <summary>
        /// Given that XML data request is received
        /// When the XML content misses horse names for some horses
        /// And the XML content misses prices for some horses
        /// Then the response contains the error code for missing horse names
        /// And the response contains the error code for missing horse prices
        /// </summary>
        [Fact]
        public void VerifyXmlResultWhenMissingHorseNamesAndPrices()
        {
            XmlDataExtractor jsonDataExtractor = new XmlDataExtractor();
            XmlDocument xmlDocument = ResourceHelper.GetXmlDocumentFromResource("Caulfield_Race1_MissingHorseNameAndPrice.xml");
            var result = jsonDataExtractor.GetHorseNamesByAscendingPrice(xmlDocument);
            Assert.Equal(2, result.HorseNames.Count);
            Assert.Equal(2, result.ValidationErrorCodes.Count);
            Assert.Contains(ErrorCode.MissingHorsePrices, result.ValidationErrorCodes);
            Assert.Contains(ErrorCode.MissingHorseNames, result.ValidationErrorCodes);

        }

    }
}
