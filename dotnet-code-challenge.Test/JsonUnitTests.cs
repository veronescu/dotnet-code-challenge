﻿using DotNetCodeChallenge.Controllers;
using DotNetCodeChallenge.Models;
using DotNetCodeChallenge.ServiceLogic;
using DotNetCodeChallenge.Test.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NSubstitute;
using System.IO;
using System.Reflection;
using Xunit;

namespace DotNetCodeChallenge.Test
{
    public class JsonUnitTests
    {
        /// <summary>
        /// When the JsonController receives a request
        /// Then the JsonController calls the method from the injected XmlDataExtractor
        /// </summary>
        [Fact]
        public void VerifyJsonCallTree()
        {
            IJsonDataExtractor jsonDataExtractor = Substitute.For<IJsonDataExtractor>();
            JsonController jsonController = new JsonController(jsonDataExtractor);
            jsonController.Post("Test String");
            jsonDataExtractor.Received(1).GetHorseNamesByAscendingPrice("Test String");
        }

        /// <summary>
        /// Given that a JSON data request is received
        /// When the JSON content has horse names for all horses
        /// And the JSON content has prices for all horses
        /// Then the response is the horse names in ascending order of their associated price
        /// </summary>
        [Fact]
        public void VerifyJsonResultWhenCorrectContent()
        {
            JsonDataExtractor jsonDataExtractor = new JsonDataExtractor();
            var input = JObject.Parse(ResourceHelper.GetContentOfResource("Wolferhampton_Race1.json"));
            var result = jsonDataExtractor.GetHorseNamesByAscendingPrice(input);
            Assert.Equal(2, result.HorseNames.Count);
            Assert.Equal("Fikhaar", result.HorseNames[0]);
            Assert.Equal("Toolatetodelegate", result.HorseNames[1]);
            Assert.Empty(result.ValidationErrorCodes);
        }

        /// <summary>
        /// Given that a JSON data request is received
        /// When the JSON content misses horse names for some horses
        /// And the JSON content has prices for all horses
        /// Then the response contains the error code for missing horse names
        /// </summary>
        [Fact]
        public void VerifyJsonResultWhenMissingHorseNames()
        {
            JsonDataExtractor jsonDataExtractor = new JsonDataExtractor();
            var input = JObject.Parse(ResourceHelper.GetContentOfResource("Wolferhampton_Race1_MissingHorseName.json"));
            var result = jsonDataExtractor.GetHorseNamesByAscendingPrice(input);
            Assert.Equal(2, result.HorseNames.Count);
            Assert.Equal("Fikhaar", result.HorseNames[0]);
            Assert.Equal("", result.HorseNames[1]);
            Assert.Single(result.ValidationErrorCodes);
            Assert.Equal(ErrorCode.MissingHorseNames, result.ValidationErrorCodes[0]);
        }

        /// <summary>
        /// Given that JSON data request is received
        /// When the JSON content has horse names for all horses
        /// And the JSON content misses prices for some horses
        /// Then the response contains the error code for missing horse prices
        /// </summary>
        [Fact]
        public void VerifyJsonResultWhenMissingPrices()
        {
            JsonDataExtractor jsonDataExtractor = new JsonDataExtractor();
            var input = JObject.Parse(ResourceHelper.GetContentOfResource("Wolferhampton_Race1_MissingPrice.json"));
            var result = jsonDataExtractor.GetHorseNamesByAscendingPrice(input);
            Assert.Equal(2, result.HorseNames.Count);
            Assert.Single(result.ValidationErrorCodes);
            Assert.Equal(ErrorCode.MissingHorsePrices, result.ValidationErrorCodes[0]);
        }
        
        /// <summary>
        /// Given that a JSON data request is received
        /// When the JSON content misses horse names for some horses
        /// And the JSON content misses prices for some horses
        /// Then the response contains the error code for missing horse names
        /// And the response contains the error code for missing horse prices
        /// </summary>
        [Fact]
        public void VerifyJsonResultWhenMissingHorseNamesAndPrices()
        {
            JsonDataExtractor jsonDataExtractor = new JsonDataExtractor();
            var input = JObject.Parse(ResourceHelper.GetContentOfResource("Wolferhampton_Race1_MissingHorseNameAndPrice.json"));
            var result = jsonDataExtractor.GetHorseNamesByAscendingPrice(input);
            Assert.Equal(2, result.HorseNames.Count);
            Assert.Single(result.ValidationErrorCodes);
            Assert.Equal(ErrorCode.MissingHorsePrices, result.ValidationErrorCodes[0]);
        }
    }
}
