# .NET Code Challenge

dotnet-code-challenge is an ASP .NET Core API that extracts lists of horse names from JSON and XML inputs, in ascending order of the price.

## Deployment instructions
* Clone the repository to your local environment
* Open a commmand line in the folder where dotnet-code-challenge.csproj is located
* Run the following command: dotnet publish -c Release --self-contained -r win7-x64
* Copy the content of sub-folder bin\Release\netcoreapp2.0\win7-x64\publish\ to a deployment location
* Open IIS and create an application pool (name it for example dotnet-code-challenge)
* Create an application (e.g dotnet-code-challenge) and point it to the deployment location
* Set the application pool to the previously created application pool

## API usage

### Endpoints
* {ROOT}/api/xml accepts POST of XML data
* {ROOT}/api/json accepts POST of JSON data

### Examples
The Postman collection provided in the root folder of the repository can be imported and used as an example for testing. There are 2 POST requests, SortXML and SortJSON.
Once imported, replace in the requests http://localhost:57937/ with your base url.

## Limitations
* A workaround had to be applied in the XML controller for an issue with XML formatters from ASP .NET Core. The solution from here did not work for version 2.2: https://stackoverflow.com/questions/46302703/post-xml-to-asp-net-core-2-0-web-api

## Future improvements
* Logging capabilities