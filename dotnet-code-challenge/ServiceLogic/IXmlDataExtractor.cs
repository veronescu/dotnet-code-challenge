﻿using DotNetCodeChallenge.Models;
using System.Collections.Generic;
using System.Xml;

namespace DotNetCodeChallenge.ServiceLogic
{
    public interface IXmlDataExtractor
    {
        Response GetHorseNamesByAscendingPrice(XmlDocument xmlDocument);
    }
}
