﻿using DotNetCodeChallenge.Models;
using System.Collections.Generic;

namespace DotNetCodeChallenge.ServiceLogic
{
    public interface IJsonDataExtractor
    {
        Response GetHorseNamesByAscendingPrice(dynamic value);
    }
}
