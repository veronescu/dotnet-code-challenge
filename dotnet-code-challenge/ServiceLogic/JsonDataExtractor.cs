﻿using DotNetCodeChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DotNetCodeChallenge.ServiceLogic
{
    public class JsonDataExtractor : IJsonDataExtractor
    {
        public Response GetHorseNamesByAscendingPrice(dynamic value)
        {
            List<JsonSelection> selections = value.RawData.Markets[0].Selections.ToObject<List<JsonSelection>>();
            List<string> horseNames = selections.OrderBy(s => s.Price).Select(s => s.Tags.name).ToList();
            List<ErrorCode> errorCodes = new List<ErrorCode>();
            if (horseNames.Any(hn => string.IsNullOrWhiteSpace(hn)))
                errorCodes.Add(ErrorCode.MissingHorseNames);
            if (selections.Any(s => s.Price == null))
                errorCodes.Add(ErrorCode.MissingHorsePrices);
            return new Response()
            {
                HorseNames = horseNames,
                ValidationErrorCodes = errorCodes
            };
        }
    }
}
