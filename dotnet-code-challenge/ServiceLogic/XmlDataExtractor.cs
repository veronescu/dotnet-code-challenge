﻿using DotNetCodeChallenge.Models;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace DotNetCodeChallenge.ServiceLogic
{
    public class XmlDataExtractor : IXmlDataExtractor
    {
        private const string horseXPath = "/meeting/races/race/horses/horse";
        private const string priceXPath = "/meeting/races/race/prices/price/horses/horse";

        public Response GetHorseNamesByAscendingPrice(XmlDocument xmlDocument)
        {
            var horseData = xmlDocument.SelectNodes(horseXPath)
                .Cast<XmlNode>()
                .Select(n => new
                {
                    Name = n.Attributes["name"]?.Value,
                    Number = n.ChildNodes
                        .Cast<XmlNode>()
                        .Where(cn => cn.Name == "number")
                        .FirstOrDefault()?.InnerText
                });

            var priceData = xmlDocument.SelectNodes(priceXPath)
                .Cast<XmlNode>()
                .Select(n => new
                {
                    Price = n.Attributes["Price"]?.Value != null ?
                        float.Parse(n.Attributes["Price"].Value) : (float?)null,
                    Number = n.Attributes["number"]?.Value
                });

            var joinedInformation = horseData.Join(priceData, h => h.Number, p => p.Number, (h, p) => new { p.Price, h.Name })
                .OrderBy(j => j.Price);

            List<ErrorCode> errorCodes = new List<ErrorCode>();
            if (horseData.Any(h => string.IsNullOrWhiteSpace(h.Name)))
                errorCodes.Add(ErrorCode.MissingHorseNames);
            if (priceData.Any(p => p.Price == null || p.Number == null))
                errorCodes.Add(ErrorCode.MissingHorsePrices);

            return new Response()
            {
                HorseNames = joinedInformation.Select(j => j.Name).ToList(),
                ValidationErrorCodes = errorCodes
            };
        }
    }
}
