﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using DotNetCodeChallenge.ServiceLogic;
using Microsoft.AspNetCore.Mvc;

namespace DotNetCodeChallenge.Controllers
{
    [Route("api/[controller]")]
    public class XmlController : Controller
    {
        private IXmlDataExtractor _xmlDataExtractor;

        public XmlController (IXmlDataExtractor xmlDataExtractor)
        {
            _xmlDataExtractor = xmlDataExtractor;
        }

        // POST api/xml
        [HttpPost]
        public IActionResult Post([FromBody]string value)
        {
            // workaround for XML formatter issue in ASP .NET Core
            using (var streamReader = new StreamReader(Request.Body))
            {
                streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                //string xml = streamReader.ReadToEnd();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(streamReader);
                return Json(_xmlDataExtractor.GetHorseNamesByAscendingPrice(xmlDocument));

            }
        }
    }
}
