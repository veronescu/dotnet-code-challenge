﻿using DotNetCodeChallenge.ServiceLogic;
using Microsoft.AspNetCore.Mvc;

namespace DotNetCodeChallenge.Controllers
{
    [Route("api/[controller]")]
    public class JsonController : Controller
    {
        private IJsonDataExtractor _jsonDataExtractor;

        public JsonController(IJsonDataExtractor jsonDataExtractor)
        {
            _jsonDataExtractor = jsonDataExtractor;
        }

        // POST api/xml
        [HttpPost]
        public IActionResult Post([FromBody]dynamic value)
        {
            return Json(_jsonDataExtractor.GetHorseNamesByAscendingPrice(value));
        }
    }
}
