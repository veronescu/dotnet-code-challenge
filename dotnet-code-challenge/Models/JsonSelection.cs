﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCodeChallenge.Models
{
    public class JsonSelection
    {
        public string Id { get; set; }
        public float? Price { get; set; }
        public JsonTag Tags{ get; set; }
    }
}
