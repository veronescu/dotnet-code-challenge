﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCodeChallenge.Models
{
    public class Response
    {
        public List<string> HorseNames { get; set; }
        public List<ErrorCode> ValidationErrorCodes { get; set; }
    }
}
