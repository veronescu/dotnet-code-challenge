﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCodeChallenge.Models
{
    public class JsonTag
    {
        public string name { get; set; }
        public string participant { get; set; }
    }
}
