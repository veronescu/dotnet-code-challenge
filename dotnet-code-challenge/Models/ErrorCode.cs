﻿namespace DotNetCodeChallenge.Models
{
    public enum ErrorCode
    {
        MissingHorseNames = 1,
        MissingHorsePrices = 2,
    }
}
